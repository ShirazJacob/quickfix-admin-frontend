import React from "react";
import { Bar } from "react-chartjs-2";
import { Chart as ChartJS, ArcElement, Tooltip, Legend, CategoryScale, LinearScale, BarElement} from 'chart.js';
import { colorPalette } from "../../constants/constants";
import { chartDataType } from "../../types/types";

interface ChartDataProps {
    chartData: chartDataType[],
}

const BarChart = (props: ChartDataProps) => {
    ChartJS.register(ArcElement, Tooltip, Legend, CategoryScale, LinearScale, BarElement);
    const generateChartData = () => {
    return { 
        labels: props.chartData.map((item) => item.id),
        datasets: [
          {
            label: "",
            data: props.chartData.map((item) => item.count),
            backgroundColor: colorPalette,
            borderColor: "white",
            borderWidth: 0.5,
          },
        ],
    }
    }

  return <Bar data={generateChartData()} />;
}

export default BarChart;
