import React from "react";
import { Pie } from "react-chartjs-2";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { colorPalette } from "../../constants/constants";
import { chartDataType } from "../../types/types";

interface ChartDataProps {
    chartData: chartDataType[],
}

const PieChart = (props: ChartDataProps) => {
    ChartJS.register(ArcElement, Tooltip, Legend);
    const generateChartData = () => {
    return { 
        labels: props.chartData.map((item) => item.id),
        datasets: [
          {
            label: "",
            data: props.chartData.map((item) => item.count),
            backgroundColor: colorPalette,
            borderColor: "white",
            borderWidth: 0.5,
          },
        ],
    }
    }

  return <Pie data={generateChartData()} />;
}

export default PieChart;
