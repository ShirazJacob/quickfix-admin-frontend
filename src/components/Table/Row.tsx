import {
  TableRow,
  TableCell,
  IconButton,
  Collapse,
  Box,
  Typography,
  TextField,
  List,
  ListSubheader,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  Button,
} from "@mui/material";
import { IProfessionalDetails } from "../../types";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import DoneIcon from "@mui/icons-material/Done";
import CloseIcon from "@mui/icons-material/Close";
import { useState } from "react";

const Row = (props: { row: IProfessionalDetails }) => {
  const { row } = props;
  const [open, setOpen] = useState<boolean>(false);

  return (
    <>
      <TableRow sx={{ "& > *": { borderBottom: "unset" } }}>
        <TableCell>
          <div
            style={{
              justifyContent: "flex-start",
              display: "flex",
              alignItems: "center",
              flexDirection: "row",
            }}
          >
            <img
              src={`https://ui-avatars.com/api/?format=svg\&rounded=true\&name=${row.first_name}\+${row.last_name}\&background=random\&bold=true`}
              width={30}
              height={30}
              style={{
                marginLeft: 5,
              }}
            />
            <span>
              {row.first_name} {row.last_name}
            </span>
          </div>
        </TableCell>
        <TableCell>{row.email}</TableCell>
        <TableCell>{"asdasd"}</TableCell>
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography variant="h6" gutterBottom component="div" dir="rtl">
                פרטי הבעל מקצוע
              </Typography>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-evenly",
                }}
              >
                <TextField
                  label={"שם מלא"}
                  value={`${row.first_name} ${row.last_name}`}
                  inputProps={{
                    "aria-readonly": true,
                  }}
                  variant="outlined"
                />
                <TextField
                  label={"כתובת"}
                  value={row.address}
                  inputProps={{
                    "aria-readonly": true,
                  }}
                  variant="outlined"
                />
                <TextField
                  label={"מייל"}
                  value={row.email}
                  inputProps={{
                    "aria-readonly": true,
                  }}
                  variant="outlined"
                />
                <TextField
                  label={"טלפון"}
                  value={row.phone_number}
                  inputProps={{
                    "aria-readonly": true,
                  }}
                  variant="outlined"
                />
                <TextField
                  label={"תוכן בקשה"}
                  value={row.description}
                  inputProps={{
                    "aria-readonly": true,
                  }}
                  multiline
                  minRows={3}
                  variant="outlined"
                />
              </div>
              <List
                subheader={
                  <ListSubheader component="div">קטגוריות</ListSubheader>
                }
                sx={{
                  width: "100%",
                  maxWidth: 360,
                  bgcolor: "background.paper",
                }}
              >
                {row.categories.map((category) => {
                  return (
                    <ListItem divider button disablePadding>
                      <ListItemAvatar>
                        <Avatar>
                          <img
                            src={category.image}
                            width={"100%"}
                            height={"100%"}
                            style={{
                              objectFit: "cover",
                            }}
                          />
                        </Avatar>
                      </ListItemAvatar>
                      <ListItemText
                        primary={category.category_desc}
                        style={{
                          textAlign: "right",
                        }}
                      />
                    </ListItem>
                  );
                })}
              </List>
            </Box>
            <Box
              style={{
                marginBottom: 10,
              }}
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-end",
                flexDirection: "row",
              }}
            >
              <Button variant="outlined" color="error" style={{ margin: 5 }}>
                <CloseIcon fontSize="small" /> דחה
              </Button>
              <Button variant="outlined" color="success" style={{ margin: 5 }}>
                <DoneIcon fontSize="small" /> קבל
              </Button>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
};

export default Row;
