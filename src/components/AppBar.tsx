import AppBar from "@material-ui/core/AppBar";
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { Box,  makeStyles } from '@material-ui/core';
import { breakpoints } from '../styles/theme';

interface AppBarProps {
  menuOpen: Boolean,
  setMenuOpen: (menuOpen: Boolean) => void
}


const useStyles = makeStyles({
  navbar: {
    // background: '#2E3B55',
    direction: 'rtl',
    background:
      'linear-gradient(218deg, rgb(46, 59, 85) 0%, rgb(46, 59, 85) 99%, rgb(46, 59, 85) 100%)',
    borderRadius: '0 0 25px 25px',
    boxShadow: '0 1px 20px 0 rgba(52, 127, 255, 0.3)',
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
    zIndex: 99,

    '& .toolbar-portal:empty': {
      display: 'none',
    },

    '& .toolbar-portal:not(:empty):not([data-preserve="preserve"]) + $mainNav ': {
      display: 'none',
    },

    '& .toolbar-portal:not(:empty)': {
      width: '100%',
    },
  },
  mainNav: {
    display: 'flex',
    justifyContent: 'center',

    padding: '4px 8px',
    width: '100%',

    '& > *': {
      flex: 1,
    },
  },
});

const ResponsiveAppBar = (props: AppBarProps) => {
  const classes = useStyles();

    return (
    <AppBar position="static"  className={`${classes.navbar}`}>
      <Container maxWidth={false}>
        <Toolbar className={`${classes.mainNav} container`} disableGutters>
          <Box display="flex" alignItems="center" marginRight="auto">
          <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={() => props.setMenuOpen(!props.menuOpen)}
          >
              <MenuIcon />
          </IconButton>
          </Box>
          <Box display="flex" justifyContent="center">
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
            style={{ color: "white", fontFamily:'CooperBlackRegular', fontSize: 36}}>
            QuickFix
          </Typography>
          </Box>
          <Box marginLeft="auto" textAlign="left"></Box>
        </Toolbar>  
      </Container>
    </AppBar>
  );
};

export default ResponsiveAppBar;