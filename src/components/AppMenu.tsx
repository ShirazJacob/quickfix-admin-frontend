import { makeStyles, createStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconDashboard from '@material-ui/icons/Dashboard';
import IconPeople from '@material-ui/icons/People';
import IconBarChart from '@material-ui/icons/BarChart';
import { useNavigate } from 'react-router-dom';
import { Typography, Box } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';

const AppMenu = () => {
  const classes = useStyles();
  const navigate = useNavigate()

  function handleClick(path: string) {
    navigate(path);
  }

  const menuItems = [{
    name: 'נתונים אסטרטגיים',
    path: '/dashboard',
    icon: <IconDashboard className={classes.Icon}/>
  }, {
    name: 'בעלי מקצוע לאישור',
    path: '/verifyProffesionals',
    icon: <IconPeople className={classes.Icon}/>,
  }, {
    name: 'דוחות',
    path: '/reports',
    icon: <IconBarChart className={classes.Icon}/>,
  }];

  return (
  <Drawer variant="permanent" classes={{root: classes.root,paper: classes.drawerPaper}}>
    <List component="nav" className={classes.appMenu} disablePadding>
      {menuItems.map((menuItem) => 
      <ListItem button divider className={classes.menuItem} onClick={() => { handleClick(menuItem.path)}}>
        <ListItemIcon className={classes.menuItemIcon}>
          {menuItem.icon}
        </ListItemIcon>
        <ListItemText primary={
            <Typography style={{ color: "black" }}>
              {menuItem.name}
           </Typography>
        } />
      </ListItem>
      )}
      <Divider />
      <Box className={classes.imgLogoBox}>
      <img src={require(`../assest/FixLogo.png`)} alt='logo' width='auto' height={50}/>
      </Box>
    </List>
  </Drawer>
)}

const drawerWidth = 260

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      '& .MuiListItem-root:before, & .MuiListItem-root:after': {
        position: 'absolute',
        backgroundColor: '#dce6fa',
        height: '1px',
        width: '100%',
      },
  
      '& .MuiListItem-root:after': {
        content: '""',
        bottom: 0,
        height: 0.5,
      },
  
      '& .MuiListItem-root:before': {
        top: 0,
      },
      '& .MuiListItem-root': {
        borderColor: '#dce6fa',
      },
  
      '& .MuiListItem-root .MuiListItemText-root .MuiTypography-root ': {
        fontSize: 17,
      },
  
      '& .MuiListItemIcon-root': {
        minWidth: 40,
      },
  
      '& .MuiListItem-root:first-child:before': {
        content: '""',
      },
    },
    appMenu: {
      width: '100%',
    },
    navList: {
      width: drawerWidth,
    },
    menuItem: {
      width: drawerWidth,
    },
    menuItemIcon: {
      color: '#F7C200',
    },
    Icon: {
      fontSize: 36
    },
    drawerPaper: {
      position: 'relative',
      whiteSpace: 'nowrap',
      width: drawerWidth,
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(4),
      background: '#F2F3F4',
      color: '#000',
    },
    imgLogoBox: {
      display: 'flex',
      justifyContent: 'center',
      padding: 20,
    },
  }),
);

export default AppMenu;
