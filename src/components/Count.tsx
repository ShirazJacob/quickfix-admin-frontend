
import React from 'react';
import {
    Card,
    Box,
    CardContent,
    Typography,
  } from '@mui/material';
  import Divider from '@material-ui/core/Divider';
  import { makeStyles, createStyles } from '@material-ui/core/styles';
import { chartDataType } from '../types';

  export const Count = (props: { item: chartDataType }) => {
    const classes = useStyles();
    // EEC86B
    return (
        <div>
          <Card style={{ height: '200px', background: 'linear-gradient(218deg, rgb(205, 155, 0) 0%, rgb(250, 250, 250) 99%, rgb(46, 59, 85) 100%)' }} sx={{ px: 1 }}>
            <CardContent>
            <Box className={classes.titleCount}>
              <Typography style={{textAlign: 'center',fontFamily: 'Segoe UI Semibold'}} variant="h5">
                {props.item.id}
              </Typography>
            </Box>
              <Divider />
            <Box>
                <Typography style={{textAlign: 'center',fontFamily: 'Segoe UI Semibold'}} variant="h2" gutterBottom noWrap>
                  {props.item.count}
                </Typography>
            </Box>
            </CardContent>
          </Card>
        </div>
    );
  }
  const useStyles = makeStyles(() =>
    createStyles({
      titleCount: {
        padding: '15px',
      },
    }),
  );
  