export interface chartDataType {
  id: string;
  count: number;
}

export interface ChartData {
  data: number[];
  lables: string[];
}

export interface IProfessionalDetails {
  phone_number: string;
  first_name: string;
  last_name: string;
  email: string;
  address: string;
  description: string;
  request_range: number;
  categories: ICategory[];
}

export interface ICategory {
  category_desc: string;
  image: string;
}
