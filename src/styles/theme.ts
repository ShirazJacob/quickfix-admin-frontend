import { createTheme } from '@material-ui/core';
import createSpacing from '@material-ui/core/styles/createSpacing';
import { ComponentsProps } from '@material-ui/core/styles/props';
import { fade } from '@material-ui/core/styles/colorManipulator';
import './App.css';
import shadows, { Shadows } from '@material-ui/core/styles/shadows';
import transitions from '@material-ui/core/styles/transitions';
import { LabComponentNameToClassKey } from '@material-ui/lab/themeAugmentation';

declare module '@material-ui/core/styles/overrides' {
  interface ComponentNameToClassKey extends LabComponentNameToClassKey {}
}

const palette = {
  primary: {
    light: '#FFD800',
    main: '#F7C200',
    dark: '#000000',
    secondary: '#6B6B69',
    contrastText: '#fff',
  },
};
const spacing = createSpacing(4);

const createShadows = (): Shadows => {
  shadows[1] = '0 2px 4px 0 rgba(0, 0, 0, 0.1)';

  return shadows as Shadows;
};
const props: ComponentsProps = {
  MuiTabs: {
    indicatorColor: 'primary',
  },
  MuiTab: {
    disableRipple: true,
  },
  MuiChip: {
    size: 'small',
    color: 'primary',
  },
};

const theme = createTheme({
  direction: 'rtl',
  props,
  spacing,
  typography: {
    fontFamily: 'Segoe UI Semibold',
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 400,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
  shadows: createShadows(),
  palette: {
    ...palette,
    text: {
      primary: palette.primary.dark,
    },
  },
});

theme.overrides = {
  MuiAppBar: {
    colorPrimary: {
      backgroundColor: '#535454',
    },
  },
  MuiToggleButton: {
    root: {
      borderRadius: '3px !important',
      border: 'none',
      padding: `${spacing(1)}px ${spacing(2)}px`,
      height: 32,

      backgroundColor: fade(palette.primary.main, 0.07),
      color: palette.primary.main,

      fontSize: 15,
      fontWeight: 'bold',

      transition: transitions.create(['color', 'background-color']),

      '&:hover, &$selected:hover': {
        backgroundColor: fade(palette.primary.light, 0.15),
      },

      '&$selected': {
        backgroundColor: palette.primary.main,
        color: '#fff',

        '&:hover': {
          backgroundColor: fade(palette.primary.light, 0.8),
        },
      },
    },
  },
  MuiToggleButtonGroup: {
    root: {
      display: 'flex',
      margin: -spacing(2.5),

      'media only screen and (min-width: 420px)': {
        justifyContent: 'space-between',
      },
    },
    grouped: {
      margin: spacing(2.5),
      flex: 1,
      flexShrink: 1,
      whiteSpace: 'nowrap',
    },
  },
  MuiRadio: {
    root: {
      '&$checked': {
        $label: {
          boxShadow: '0 1px 5px 0 rgba(52, 127, 255, 0.3)',
          borderRadius: '50%',
        },
      },
    },
  },
  MuiFormGroup: {
    root: {
      alignItems: 'flex-start',
    },
  },
  MuiFormControl: {
    root: {
      '&.disabled.zimun-picker': {
        pointerEvents: 'none',
        borderColor: '#FFFCE8',

        '&  input': {
          color: palette.primary.dark,
        },

        '& .MuiIconButton-root': {
          display: 'none',
        },
      },

      '&.zimun-picker': {
        border: '2px solid #dce6fa',
        borderRadius: 3,
        boxShadow: '0 2px 5px 0 rgba(52, 127, 255, 0.1)',

        padding: '6px 16px',
        cursor: 'pointer',

        transition: transitions.create(['border-color']),

        '&:hover': {
          borderColor: palette.primary.dark,
        },

        '& .MuiInputAdornment-root': {
          marginRight: 8,
          width: 16,
        },
        '&.with-label .MuiInputAdornment-root': {
          marginTop: -16,
        },

        '& .MuiFormHelperText-root.Mui-error': {
          position: 'absolute',
          top: '100%',
          right: 0,
        },

        '& label': {
          color: palette.primary.dark,
          left: 16,
          top: 12,

          fontSize: 18,
        },

        '& input': {
          color: palette.primary.main,
          fontWeight: 'bold',
          cursor: 'pointer',
        },

        '& .MuiInput-underline:after, & .MuiInput-underline:before': {
          content: 'none',
        },
      },
    },
  },
  MuiOutlinedInput: {
    root: {
      border: 'none',
      fontSize: 18,
      fontWeight: 600,
      color: palette.primary.dark,
      transition: transitions.create(['background-color']),

      '&:hover': {
        backgroundColor: '#fff',
      },
      '$root&:hover $notchedOutline': {
        boxShadow: '0 -1px 20px 0 rgba(255,255,0,0.3)',
        border: `2px solid ${fade(palette.primary.main, 0.3)}`,
      },
      '$root&$focused $notchedOutline': {
        border: `2px solid ${fade(palette.primary.main, 0.5)}`,
      },
      '$root&$disabled $notchedOutline': {
        boxShadow: 'none',
        borderColor: 'rgba(0, 0, 0, 0.12)',
      },
      $focused: {
        boxShadow: 'none',
        backgroundColor: '#fff',
      },
      '> svg': {
        marginLeft: spacing(2),
      },
    },
    input: {
      padding: `${spacing(3)}px ${spacing(2)}px`,
    },
    inputAdornedStart: {
      paddingLeft: spacing(4),
    },
    notchedOutline: {
      borderRadius: 4,
      border: `2px solid`,
      transition: transitions.create(['border-color', 'box-shadow']),
      borderColor: '#FFFCE8',
    },
  },
  MuiButton: {
    root: {
      fontSize: 16,
      fontWeight: 'bold',
      borderRadius: 4,
      padding: `${spacing(2)}px ${spacing(4)}px`,
      margin: `${spacing(4)}px ${spacing(2)}px ${spacing(2)}px ${spacing(0)}px`,

      '&.rounded': {
        borderRadius: 500,
      },
      '&.gradient': {
        boxShadow: '0 5px 10px 0 rgba(255,255,0,0.3)',
        backgroundImage: 'linear-gradient(216deg, #EDE200 112%, #FFCC00 -35%)',
      },
    },
    contained: {
      '&:hover': {
        backgroundColor: '#ececec',
      },
    },
    containedPrimary: {
      color: palette.primary.contrastText,
      background:
        'linear-gradient(257deg, rgba(255,200,0,0.6) 0%, rgba(255,230,1,1) 90%)',
      boxShadow: '0 5px 10px 0 rgba(255,255,0,0.3)',
      transition: transitions.create(['background-color', 'box-shadow']),

      $label: {
        color: palette.primary.contrastText,
      },
      '&:hover': {
        backgroundColor: palette.primary.light,
      },
      '&$disabled': {
        background: 'rgba(0, 0, 0, 0.12)',
      },
    },
    sizeSmall: {
      padding: `${spacing(1)}px ${spacing(5)}px`,
      fontSize: 15,
      height: 32,
    },
    sizeLarge: {
      height: 50,
      fontSize: 18,
      fontWeight: 'bold',
    },
    outlinedPrimary: {
      color: palette.primary.main,
      border: `2px solid ${palette.primary.main}`,
      boxShadow: '0 1px 5px 0 rgba(255,255,0,0.3)',
      transition: transitions.create([
        'background-color',
        'box-shadow',
        'border-color',
      ]),

      '&:hover': {
        borderWidth: 2,
        backgroundColor: fade(palette.primary.main, 0.04),
      },
    },
    label: {
      color: palette.primary.main,
      transition: transitions.create(['color']),

      '$containedPrimary &': {
        color: palette.primary.contrastText,
      },
    },
  },
  MuiDialog: {
    paper: {
      width: '100%',
      margin: 20,
    },
    paperWidthSm: {
      maxWidth: 370,
    },
    paperFullWidth: {
      width: 'calc(100% - 20px)',
    },
  },
  MuiDialogTitle: {
    root: {
      padding: '18px',
    },
  },
  MuiDialogContent: {
    root: {
      padding: '16px 18px',
    },
  },
  MuiDialogActions: {
    root: {
      flexWrap: 'wrap',
      padding: '12px 14px',

      '& > *': {
        flexGrow: 1,
        margin: '4px !important',
      },
    },
  },
  MuiDivider: {
    root: {
      backgroundColor: '#FAF6DC',
      height: 1,
    },
  },
  MuiTypography: {
    root: {
      color: palette.primary.dark,
      fontWeight: 'normal',
    },
    colorTextSecondary: {
      color: '#fff',
    },
    h2: {
      fontSize: 22,
      fontWeight: 'bold',
    },
    h3: {
      fontSize: 18,
    },
    subtitle1: {
      fontSize: 18,
      fontWeight: 'bold',
      lineHeight: 1.28,
      marginBottom: 2,
    },
    body1: {
      fontSize: 18,
    },
  },
  MuiPaper: {
    root: {
      color: palette.primary.dark,
    },
  },
  MuiChip: {
    sizeSmall: {
      padding: `0 ${spacing(1)}px`,
    },
    labelSmall: {
      fontSize: 14,
      fontWeight: 'bold',
    },
    colorPrimary: {
      color: palette.primary.main,
      backgroundColor: fade(palette.primary.main, 0.07),
    },
  },
  MuiTabs: {
    scroller: {
      backgroundColor: fade(palette.primary.main, 0.07),
      borderRadius: 50,
      padding: 6,

      button: {
        zIndex: 2,
      },
    },
    indicator: {
      height: `calc(100% - 12px)`,
      borderRadius: 50,
      backgroundColor: 'transparent',
      top: '50%',
      transform: 'translateY(-50%)',
      boxShadow: '0 5px 10px 0 rgba(255,255,0,0.3)',
      backgroundImage:
        'linear-gradient(257deg, rgba(255,200,0,0.6) 0%, rgba(255,230,1,1) 90%)',
    },
  },
  MuiTab: {
    root: {
      zIndex: 2,
      padding: spacing(1),
      minHeight: 38,
      minWidth: '0 !important',

      '$root&$selected $wrapper': {
        color: '#fff',
      },

      '$root $ripple': {
        display: 'none',
      },
    },
    wrapper: {
      color: palette.primary.main,
      fontWeight: 'bold',
      fontSize: 16,

      transition: transitions.create('color'),
    },

    textColorInherit: {
      opacity: 1,
    },
  },

  MuiListItem: {
    root: {
      fontSize: 20,
      fontWeight: 'bold',
    },
  },
  MuiAccordion: {
    root: {
      boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.05)',
      border: '1px solid #FFFCE8',
    },
  },
  MuiAccordionDetails: {
    root: {
      padding: theme.spacing(0, 4, 5),
    },
  },
  MuiAccordionSummary: {
    root: {
      height: 60,
      padding: theme.spacing(0, 4),
    },
    expandIcon: {
      marginRight: -4,

      '&$expanded': {
        transform: 'rotate(90deg)',
      },
    },
  },
  MuiTableCell: {
    root: {
      '.border-less &': {
        border: 0,

        '&:first-child': {
          paddingLeft: 0,
        },
        '&:last-child': {
          paddingRight: 0,
        },
      },
    },
    head: {
      fontWeight: 600,
    },
    sizeSmall: {
      padding: '4px 10px',
    },
  },
};
export const { breakpoints } = theme;

export default theme;
