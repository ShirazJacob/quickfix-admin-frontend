import React, { useEffect, useState } from "react";
import SignIn from "./pages/SignIn";
import { API_URL, TOKEN_KEY } from "./constants/constants";
import { ThemeProvider } from "@material-ui/core";
import theme from "./styles/theme";
import Home from "./pages/Home";
import "./styles/App.css";
import "./types/index";
import rtlPlugin from "stylis-plugin-rtl";
import { CacheProvider } from "@emotion/react";
import createCache from "@emotion/cache";
import { prefixer } from "stylis";

// Create rtl cache
const cacheRtl = createCache({
  key: "muirtl",
  stylisPlugins: [prefixer, rtlPlugin],
});

const App = () => {
  const [isLogin, setIsLogin] = useState<Boolean>(false);

  const checkLogin = () => {
    let token = localStorage.getItem(TOKEN_KEY);
    if (token) {
      // Verify token
      fetch(`${API_URL}/api/isAuth`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          authorization: "Bearer " + token,
        },
      })
        .then(async (res) => {
          const jsonRes = await res.json();
          if (res.status !== 200) {
            setIsLogin(false);
          } else {
            // refresh token
            localStorage.setItem(TOKEN_KEY, jsonRes.token);
            setIsLogin(true);
          }
        })
        .catch((err) => {
          setIsLogin(false);
        });
    }
  };

  useEffect(() => {
    checkLogin();
  }, []);

  return (
    <CacheProvider value={cacheRtl}>
      <ThemeProvider theme={theme}>
        {!isLogin && <SignIn setIsLogin={setIsLogin} />}
        {isLogin && <Home />}
      </ThemeProvider>
    </CacheProvider>
  );
};

export default App;
