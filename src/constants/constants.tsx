export const fonts = {
  CooperBlackRegular: "CooperBlackRegular",
  SegoeUI: "SegoeUI",
};

export enum screens {
  Login = "Login",
}
export const SERVER_HOSTNAME = 'quickfix.cs.colman.ac.il'; 
export const CLIENT_HOSTNAME = "localhost";

export const SERVER_PORT = process.env.SERVER_PORT || 3000;
export const CLIENT_PORT = process.env.CLIENT_PORT || 80;
export const TOKEN_KEY = "TOKEN";
export const API_URL = "http://" + SERVER_HOSTNAME + ":" + SERVER_PORT;
export const CLIENT_URL = "http://" + CLIENT_HOSTNAME + ":" + CLIENT_PORT;
export const colorPalette = ["#A3A948","#EDB92E","#F85931","#CE1836","#009989","#C44D58","#C6A49A",
"#E8D5B9","#EFFFCD","#FFAAA6","#005F6B","#E84A5F","#542437","#83AF9B","#F2D694","#CBE86B"]; 
export enum Role {
  Client = "CLIENT",
  Professional = "PROFESSIONAL",
  Admin = "ADMIN",
  NoRole = "noRole",
}

export enum Request_status {
  CREATED = 'CREATED',
  IN_PROGRESS = 'IN_PROGRESS',
  DONE = 'DONE',
  CANCELED = 'CANCELED',
}
