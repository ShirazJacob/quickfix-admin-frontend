import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import { Box, Button, Typography } from '@material-ui/core';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Paper from '@mui/material/Paper';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { API_URL, CLIENT_URL, Role, TOKEN_KEY } from '../constants/constants';

interface signInProps {
    setIsLogin: (isLogin: boolean) => void;
}

const SignIn = (props: signInProps) => {
    const [error, setError] = React.useState<String>("");
    
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);

        const payload = { 
            email: data.get('email'),
            password: data.get('password'),
        };

        if(!payload.email || !payload.password) {
            setError('וודא שהזנת פרטים');
        } else {
            fetch(`${API_URL}/api/auth/admin/signin`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(payload),
            })
            .then(async (res) => {
                const jsonRes = await res.json();
                console.log(res.status);
        
                if (res.status !== 200) {
                    if (res.status === 500) {
                        setError('הייתה בעיה בהתחברות, נסה שוב');
                    } else {
                        setError('אימייל או סיסמא לא נכונים, נסה שוב');
                    }
                } else {
                    if(jsonRes.role === Role.Admin) {
                        localStorage.setItem(TOKEN_KEY, jsonRes.token);
                        props.setIsLogin(true);
                    } else {
                        setError('מצטערים, ההרשאות שלך אינן מתאימות');
                    }
                }
            })
            .catch((err) => {
                setError('מצטערים, הייתה בעיה בהתחברות, נסה שוב');
            });
        }
  };

  return (
      <Grid container component="main" direction="column" style={{  height: '100vh'}}>
        <CssBaseline />
          <Grid
          item       
          md={12}
          style={{backgroundImage: `url(${CLIENT_URL + '/yellow_backgroud.jpg'})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundPosition: 'center',}}>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}>
              <Typography style={{fontFamily:'CooperBlackRegular'}} component="h1" variant="h1">
                  QuickFix.
              </Typography>
              <img src={require(`../assest/FixLogo.png`)} alt='logo'/>
            </Box>
          </Grid>
        <Grid item md={12} component={Paper} elevation={6} square>
          
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Avatar sx={{ width: 56, height: 56, m:2, bgcolor: 'primary.secondary' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography  component="h1" variant="h5">
              התחברות
            </Typography>
            <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label="אימייל"
                name="email"
                autoComplete="email"
                autoFocus
              />
              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label="סיסמה"
                type="password"
                id="password"
                autoComplete="current-password"
              />
              <Button fullWidth type="submit" color="primary" variant="contained">התחבר</Button>   
              <Box sx={{ my: 2,
                         mx: 4,
                         display: 'flex',
                         flexDirection: 'column', 
                         alignItems: 'center'}}>
              <Typography component="h1" variant="h6" style={{color: 'red'}}>
                {error}
              </Typography>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
  );
}

export default SignIn;