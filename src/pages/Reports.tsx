import {
    Grid,
    Card,
    Box,
    CardContent,
    Typography,
  } from '@mui/material';
import { useEffect, useState } from 'react';
import PieChart from '../components/Charts/PieChart';
import BarChart from '../components/Charts/BarChart';
import Divider from '@material-ui/core/Divider';
import { TOKEN_KEY, API_URL } from '../constants/constants';
import axios from 'axios';
import { chartDataType } from '../types/types';

const Reports = () => {
    const token = localStorage.getItem(TOKEN_KEY);
    const [proffesionalsByCategory, setProffesionalsByCategory] = useState<chartDataType[]>();
    const [requestByCategory, setRequestByCategory] = useState<chartDataType[]>();
    
    const configReq =  
    { 
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "authorization": 'Bearer ' + token,
        }
    }; 

    const proffesionalsByCategoryAPI = `${API_URL}/api/admin/proffesionalsByCategoryId`;
    const requestByCategoryAPI = `${API_URL}/api/admin/requestsByCategoryId`;
    const getProffesionalsByCategoryId = axios.get(proffesionalsByCategoryAPI, configReq);
    const getRequestByCategory = axios.get(requestByCategoryAPI ,configReq);

    const fetchData = () => {
        axios.all([getProffesionalsByCategoryId, getRequestByCategory]).then(axios.spread((...responses) => {
            setProffesionalsByCategory(responses[0].data);
            setRequestByCategory(responses[1].data);
          })).catch(errors => {
              console.log(errors);
          });
    }

    useEffect(() => fetchData(), []);
    
  return (
    <Grid
    container
    direction="row"
    justifyContent="center"
    alignItems="stretch"
    spacing={3}
  >
    <Grid item xs={4}>
    <Card style={{ height: 'auto', backgroundColor:'white' }} sx={{ px: 1 }}>
            <CardContent>
            <Box>
            <Typography style={{textAlign: 'center',fontFamily: 'Segoe UI Semibold'}} variant="h6">בעלי מקצוע לפי קטגוריה</Typography>
            </Box>
              <Divider />
            <Box>
            {proffesionalsByCategory && <PieChart chartData={proffesionalsByCategory}/>}
            </Box>
            </CardContent>
          </Card>
  </Grid>
   <Grid item xs={7}>
   <Card style={{ height: 'auto', backgroundColor:'white' }} sx={{ px: 1 }}>
            <CardContent>
            <Box>
            <Typography style={{textAlign: 'center',fontFamily: 'Segoe UI Semibold'}} variant="h6">מספר בקשות לפי קטגוריית בעל מקצוע</Typography>
            </Box>
              <Divider />
            <Box>
                {requestByCategory && <BarChart chartData={requestByCategory}/>}
            </Box>
            </CardContent>
          </Card>
  </Grid>
 </Grid>
  );
}

export default Reports;
