import React, { useEffect, useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { IProfessionalDetails } from "../types";
import { API_URL, TOKEN_KEY } from "../constants/constants";
import { TablePagination, LabelDisplayedRowsArgs } from "@mui/material";
import Row from "../components/Table/Row";
import TablePaginationActions from "../components/Table/TablePaginationActions";

const ApprovalProfessionals = () => {
  const [professionals, setProfessionals] = useState<IProfessionalDetails[]>(
    []
  );

  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);
  const [emptyRows, setEmptyRows] = useState<number>(0);

  // Avoid a layout jump when reaching the last page with empty rows.
  useEffect(() => {
    setEmptyRows(
      page > 0
        ? Math.max(0, (1 + page) * rowsPerPage - professionals.length)
        : 0
    );
  }, [page]);

  useEffect(() => {
    getProfessionals();
  }, []);

  const getProfessionals = async () => {
    let token = localStorage.getItem(TOKEN_KEY);
    if (token) {
      const res = await fetch(`${API_URL}/api/admin/waitingAppPros`, {
        method: "GET",
        headers: {
          authorization: "Bearer " + token,
        },
      });

      const professionalsReqs: IProfessionalDetails[] = await res.json();
      setProfessionals(professionalsReqs);
    }
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <Paper>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>שם</TableCell>
              <TableCell>מייל</TableCell>
              <TableCell>תאריך יצירת בקשה</TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {professionals
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((professional) => (
                <Row key={professional.phone_number} row={professional} />
              ))}
            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        dir="rtl"
        labelDisplayedRows={(paginationInfo: LabelDisplayedRowsArgs) => {
          return (
            <div>
              מוצגים {paginationInfo.from} - {paginationInfo.to} מתוך{" "}
              {paginationInfo.count}
            </div>
          );
        }}
        labelRowsPerPage={"מס שורות להצגה:"}
        rowsPerPageOptions={[5, 10, 25]}
        count={professionals.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        SelectProps={{
          native: true,
        }}
        ActionsComponent={TablePaginationActions}
      />
    </Paper>
  );
};

export default ApprovalProfessionals;
