import { Grid } from '@mui/material';
import { Count } from '../components/Count';
import { API_URL, Request_status, Role, TOKEN_KEY } from '../constants/constants';
import { chartDataType } from '../types';
import axios from 'axios';
import { useEffect, useState } from 'react';

const Dashboard = () => {
  const [countData, setCountData] = useState<chartDataType[]>();
  const token = localStorage.getItem(TOKEN_KEY);
  
  const configReq =  
  { 
      method: "POST",
      headers: {
          "Content-Type": "application/json",
          "authorization": 'Bearer ' + token,
      }
  }; 

  const requestByStatusAPI = `${API_URL}/api/admin/getRequsetsByStatus`;
  const requestsByUserAPI = `${API_URL}/api/admin/getRequsetsByUser`;
  const getUsersByRoleAPI = `${API_URL}/api/admin/getUsersByRole`;

  const getOpenRequest = axios.post(requestByStatusAPI, { status: Request_status.IN_PROGRESS }, configReq);
  const getDoneRequest = axios.post(requestByStatusAPI, { status: Request_status.DONE }, configReq);
  const getCancelledRequest = axios.post(requestByStatusAPI, { status: Request_status.CANCELED }, configReq);
  const getRequestsByUser = axios.get(requestsByUserAPI, configReq);
  const getAllProfessionals = axios.post(getUsersByRoleAPI, { role: Role.Professional }, configReq);
  const getAllClients = axios.post(getUsersByRoleAPI, { role: Role.Client }, configReq);

  const fetchData = () => {
    let data: chartDataType[] = [];
    axios.all([getOpenRequest, getDoneRequest, getCancelledRequest, getRequestsByUser, getAllProfessionals, getAllClients]).then(axios.spread((...responses) => {
        data.push({id: 'בקשות פתוחות', count: responses[0].data});
        data.push({id: 'בקשות שנפתרו ע"י בעל מקצוע', count: responses[1].data});
        data.push({id: 'בקשות שבוטלו', count: responses[2].data});
        data.push({id: 'מספר הלקוחות שהזינו תקלה', count: responses[3].data});
        data.push({id: 'מספר בעלי מקצוע רשומים', count: responses[4].data});
        data.push({id: 'מספר לקוחות רשומים', count: responses[5].data});

        setCountData(data);

        })).catch(errors => {
            console.log(errors);
    });
  }

  useEffect(() => fetchData(), []);


//   const data = [{name: 'בעלי מקצוע רשומים', count: 32},
//   {name: 'לקוחות רשומים' , count: 105},
// ];
  return (
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="stretch"
          spacing={3}
        >
        {countData && countData.map(item =>  
          <Grid item xs={3}>
             <Count item={item}/>
          </Grid>)
        }
        </Grid>
  );
}

export default Dashboard;
