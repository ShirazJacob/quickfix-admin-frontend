import React, { useState } from 'react'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import Container from '@material-ui/core/Container'
import AppMenu from '../components/AppMenu'
import Dashboard from './Dashboard'
import ResponsiveAppBar from '../components/AppBar'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Reports from './Reports'
import ApprovalProfessionals from './ApprovalProfessionals'

const Home = () => {
  const classes = useStyles();
  const [menuOpen, setMenuOpen] = useState<Boolean>(true);

  return (
    <BrowserRouter>
    <ResponsiveAppBar menuOpen={menuOpen} setMenuOpen={setMenuOpen}/>
    <div className={clsx('App', classes.root)}>
      <CssBaseline />
      { menuOpen && <AppMenu />}
      <main className={classes.content}>
        <Container maxWidth="lg" className={classes.container}>
            <Routes>
              <Route path="/" element={<div></div>} />
              <Route path="/verifyProffesionals" element={<ApprovalProfessionals />} />
              <Route path="/dashboard" element={<Dashboard />} />
              <Route path="/reports" element={<Reports />} />
            </Routes>
        </Container>
      </main>
    </div>
    </BrowserRouter>
  )
}

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    direction: 'rtl'
  },
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
}))

export default Home;
